// @ts-ignore
import { gql } from "apollo-boost";

export const ALL_PRODUCTS = gql`
    query {
        getProductsListResource(searchTerm: "*", attrs:"sku,salePrice,listPrice,availability,manufacturer,image,inStock,packingUnit") {
        amount
        total
        elements {
                description
                title
                uri
                attributes {name type value}
            }
        }
    }
`;

export const GET_PRODUCT = gql`
    query ($id: String!){
        getProductSingle(productKey: $id) {
            productName
            attributes {
                name
                value
                type
            }
            availability
            availableStock
            defaultCategory {
                name
                uri
            }
            images {
                effectiveUrl
                primaryImage
                name
                imageActualWidth
                imageActualHeight
                typeID
                viewID
            }
            mastered
            manufacturer
            name
            listPrice {
                currency
                currencyMnemonic
                value
                priceText
            }
            price
            productName
            reviews
            sku
            shortDescription
            longDescription
        }
    }
`;