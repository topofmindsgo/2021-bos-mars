// @ts-ignore
declare type Price = {
    listPrice: {
        currency: string,
        currencyMnemonic: string,
        value: number,
        priceText: any
    }
}

declare type Product = {
    sku: string;
    hasCampaign: boolean;
    name: string;
    manufacturer: string;
    description: string;
    images: Array<string>;
    price: Price;
    unit: string;
    labels?: Array<Label>;
    quantity?: number;
    variants?: Object;
}

declare type Variant = {
    id: string,
    value: string,
    inStock: boolean
}

declare type Label = {
    name: string;
    description: string;
    type: string;
    id: string;
}