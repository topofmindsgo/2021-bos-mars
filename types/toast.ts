// @ts-ignore

declare type ToastType = {
    title: string,
    description?: string,
    status: string
}