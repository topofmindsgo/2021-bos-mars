declare type ModalType = {
    modalContent?: string | string [],
    modalTitle?: string | string [],
    id?: string | string [],
    returnHref: string | string []
}