// @ts-ignore
declare type Cart = {
    items: Array<Product>,
    currency: string,
    sum: number,
    quantity: number
}