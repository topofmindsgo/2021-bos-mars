export const MODAL_CONTENT_PRODUCT = "MODAL_CONTENT_PRODUCT";
export const MODAL_CONTENT_EDITORIAL = "EDITORIAL";


// Actions
export const CART_ADD = "CART_ADD";
export const CART_REMOVE = "CART_REMOVE";
export const CART_CLEAR = "CART_CLEAR";
export const TOAST_SHOW = "TOAST_SHOW";
export const TOAST_CLOSE_LAST = "TOAST_CLOSE_LAST";
export const TOAST_CLEAR = "TOAST_CLEAR";



