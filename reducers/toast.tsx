import {TOAST_CLEAR, TOAST_CLOSE_LAST, TOAST_SHOW} from "../types/types";

const toastState = {lastToast: {}, action: ''};

const toastReducer = (state = toastState, {type, payload}) => {

    switch (type) {
        case TOAST_SHOW:
            return {lastToast: payload, action: type}
        case TOAST_CLOSE_LAST:
            return {...state, action: type};
        case TOAST_CLEAR:
            return {...state, action: type};
        default:
            return state;
    }

}

export default toastReducer;