import { combineReducers } from "redux";
import toastReducer from "./toast";
import cartReducer from "./cart";

// Combine reducers
const reducers = {
    cart: cartReducer,
    toast: toastReducer
}

export default combineReducers(reducers);