import * as types from "../types/types";

const defaultCartState:Cart = {
    items: [],
    currency: 'SEK',
    quantity: 0,
    sum: 0
}

const cartReducer = (state = defaultCartState, { type, payload }) => {

    switch (type) {
        case types.CART_ADD:
            return { ...addToCart({...state}, payload.sku, payload.quantity) };

        case types.CART_REMOVE:
            return { ...removeFromCart({...state}, payload.sku, payload.quantity) };

        case types.CART_CLEAR:
            return defaultCartState;

        default:
            return state;
    }
}


const removeFromCart = (cart, item, quantity) => {

    let cartItems = [...cart.items];
    const itemIndex = cart.items.findIndex(cartItem => cartItem.sku === item.sku);

    cartItems[itemIndex] = { ...cartItems[itemIndex], quantity: cartItems[itemIndex].quantity - quantity };
    const sum = cart.sum - (cartItems[itemIndex].sku.listPrice.value * quantity);

    const newQuantity = cart.quantity - quantity;

    if(cartItems[itemIndex].quantity <= 0) {
        cartItems = cartItems.filter(cartItem => cartItem.sku !== item.sku);
    }

    return {
        ...cart,
        items: cartItems,
        quantity: newQuantity,
        sum
    }
}


const addToCart = (cart, item, quantity) => {

    let cartItems = [...cart.items];
    const itemIndex = cartItems.findIndex(cartItem => cartItem.sku === item.sku);

    // Modify existing or add new cart object
    if(itemIndex >= 0) {
        cartItems[itemIndex] = {...item, quantity: cartItems[itemIndex].quantity + quantity}
    }else {
        cartItems = [...cartItems, { ...item, quantity }];
    }

    /*
    const skuPrice = item.price?.campaignPrice ?
        item.price?.campaignPrice :
        item.price.price;
     */

    const skuPrice = item.sku.listPrice.value;
    const cartSum = cart.sum + skuPrice;

    return {
        ...cart,
        items: cartItems,
        quantity: cart.quantity + quantity,
        sum: cartSum
    }
}

export default cartReducer;