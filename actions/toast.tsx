import {TOAST_CLEAR, TOAST_CLOSE_LAST, TOAST_SHOW} from "../types/types";

export const toastShow = (toast: ToastType) => ({type: TOAST_SHOW, payload: toast});
export const toastCloseLast = () => ({type: TOAST_CLOSE_LAST});
export const toastClear = () => ({type: TOAST_CLEAR});