import {addToCart, removeFromCart, clearCart} from "./cart";
import {toastShow, toastCloseLast, toastClear} from "./toast";

export const toastActions = {
    show: toastShow,
    closeLast: toastCloseLast,
    clear: toastClear
}

export const cartActions = {
    add: addToCart,
    remove: removeFromCart,
    clear: clearCart
}