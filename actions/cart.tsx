import * as types from "../types/types";

export const addToCart = (sku: Product, quantity: number) => ({ type: types.CART_ADD, payload: { sku, quantity } });
export const removeFromCart = (sku: Product, quantity: number) => ({ type: types.CART_REMOVE, payload: { sku, quantity } });
export const clearCart = () => ({ type: types.CART_CLEAR });