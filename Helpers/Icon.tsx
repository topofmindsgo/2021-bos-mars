import React from "react";

/*
* Open source icons:
* Eva Icons https://akveo.github.io/eva-icons/
*/

import Arrow from "../public/icons/arrow-outline.svg";
import Car from "../public/icons/car-outline.svg";
import Check from "../public/icons/check.svg";
import Check2 from "../public/icons/check-2.svg";
import Heart from "../public/icons/heart-outline.svg";
import HeartFilled from "../public/icons/heart.svg";
import Menu from "../public/icons/menu-outline.svg";
import Chat from "../public/icons/chat.svg";
import User from "../public/icons/person-outline.svg";
import Pin from "../public/icons/pin-outline.svg";
import Refresh from "../public/icons/refresh-outline.svg";
import Search from "../public/icons/search-outline.svg";
import Cart from "../public/icons/shopping-cart-outline.svg";
import Trash from "../public/icons/trash-outline.svg";
import Close from "../public/icons/close.svg";
import Plus from "../public/icons/plus-circle-outline.svg";
import Minus from "../public/icons/minus-circle-outline.svg";
import ArrowFilled from "../public/icons/arrow-up-filled.svg";

const Icon = (props) => {
    return(
        <>
            { iconSelector(props) }
        </>
    )
}


const iconSize = (size: string) => {
    switch (size) {
        case "sm":
            return "18px";
        case "md":
            return "24px";
        case "lg":
            return "32px";
        case "xl":
            return "46px";
        default:
            return "24px";
    }
}

const iconSelector = (props) => {
    const {name, size, color} = props;
    const iconStyle = { fill: color, width: iconSize(size) }

    switch (name) {
        case 'cart':
            return <Cart style={iconStyle} />;
        case 'arrow':
            return <Arrow style={iconStyle} />;
        case 'car':
            return <Car style={iconStyle} />;
        case 'heart':
            return <Heart style={iconStyle} />;
        case 'heart-filled':
            return <HeartFilled style={iconStyle} />;
        case 'menu':
            return <Menu style={iconStyle} />;
        case 'chat':
            return <Chat style={iconStyle} />;
        case 'user':
            return <User style={iconStyle} />;
        case 'pin':
            return <Pin style={iconStyle} />;
        case 'refresh':
            return <Refresh style={iconStyle} />;
        case 'search':
            return <Search style={iconStyle} />;
        case 'trash':
            return <Trash style={iconStyle} />;
        case 'check':
            return <Check style={iconStyle} />;
        case 'check-2':
            return <Check2 style={iconStyle} />;
        case 'close':
            return <Close style={iconStyle} />;
        case 'plus':
            return <Plus style={iconStyle} />;
        case 'minus':
            return <Minus style={iconStyle} />;
        case 'arrow-filled':
            return <ArrowFilled style={iconStyle} />;
        default:
            return '';
    }
}

export default Icon;