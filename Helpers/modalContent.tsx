import {MODAL_CONTENT_EDITORIAL, MODAL_CONTENT_PRODUCT} from "../types/types";
import ProductDetails from "../components/ProductDetails/ProductDetails";

export const modalContentHelper = (modalContent, id) => {
    switch (modalContent) {
        case MODAL_CONTENT_PRODUCT:
            return <ProductDetails id={id} />;
        case MODAL_CONTENT_EDITORIAL:
            return "";
        default:
            return false;
    }
}