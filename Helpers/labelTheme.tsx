const labelTheme = (type:string) => {
    switch (type) {
        case "discount":
            return "red";
        default:
            return "teal";
    }
}

export default labelTheme;