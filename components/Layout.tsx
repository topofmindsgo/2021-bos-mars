import React from "react";
import AppHeader from "./Header/Header";
import AppFooter from "./Footer/Footer";
import ToastMessage from "./Toast/ToastMessage";
import ModalView from "./Modal/ModalView";
import {Box} from "@chakra-ui/layout";

const Layout: React.FunctionComponent = (props) => {
    return(
        <>
            <AppHeader/>
            <Box as="main" mt="5" minH="100vh">
                {props.children}
            </Box>
            <AppFooter/>
            <ToastMessage/>
            <ModalView />
        </>
    )
}

export default Layout;
