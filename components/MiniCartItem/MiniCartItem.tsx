import React from "react";
import {useDispatch} from "react-redux";
import {cartActions} from "../../actions";
import {Box, Button, Image, Text} from "@chakra-ui/react";
import Icon from "../../Helpers/Icon";
import ProductPrice from "../ProductPrice/ProductPrice";
import ProductQuantity from "../ProductQuantity/ProductQuantity";

const MiniCartItem:React.FunctionComponent<Product> = (props) => {

    const {productName, listPrice, images, manufacturer} = props.sku;
    const cartItem = props;
    const dispatch = useDispatch();

    const onDeleteRowClick = () => {
        dispatch(cartActions.remove(cartItem, cartItem.quantity));
    }

    return (<>
        <Box d="flex" mb="6">

            <Image
                src={images[0].effectiveUrl}
                alt={images[0].name}
                width="65px"
                maxH="75px"
                mr="2"
            />

            <Box ml="2" width="100%">
                <Text noOfLines={1} fontSize="sm" fontWeight="bold">{manufacturer}</Text>
                <Text noOfLines={1} fontSize="sm">{productName}</Text>
                <ProductPrice price={listPrice.value} />

                <Box d="flex" justifyContent="space-between" mt="1">
                    <ProductQuantity sku={cartItem} />

                    <Button variant="ghost" size="xs" onClick={onDeleteRowClick}>
                        <Icon name='trash' size='sm' />
                    </Button>
                </Box>

            </Box>
        </Box>
    </>)
}

export default MiniCartItem;