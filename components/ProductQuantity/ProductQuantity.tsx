import React from "react";
import {useDispatch, useSelector} from "react-redux";
import {cartActions} from "../../actions";
import {Box, Button, Text} from "@chakra-ui/react";
import Icon from "../../Helpers/Icon";

interface Props {
    sku: Product
}

const ProductQuantity:React.FunctionComponent<Props> = (props) => {
    const {sku} = props;
    const dispatch = useDispatch();
    const cart = useSelector((state) => state.cart);
    const quantity = cart.items.find(item => item.sku === sku.sku).quantity;

    const onQuantityAdd = (quantity: number) => {
        dispatch(cartActions.add(sku, quantity))
    }

    const onQuantitySubtract = (quantity: number) => {
        dispatch(cartActions.remove(sku, quantity))
    }

    return (
        <>
            <Box d="flex">
                <Button variant="ghost" size="xs" onClick={() => onQuantitySubtract(1)}>
                    <Icon name='minus' size='sm' />
                </Button>

                <Text mx="1">{quantity} {sku.unit}</Text>

                <Button variant="ghost" size="xs" onClick={() => onQuantityAdd(1)}>
                    <Icon name='plus' size='sm' />
                </Button>
            </Box>
        </>
    )
}

export default ProductQuantity;