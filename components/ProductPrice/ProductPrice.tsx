import React from "react";
import { Box, Text } from "@chakra-ui/react";

declare type Price = {
  salePrice: PriceItem;
  listPrice: PriceItem;
};

declare type PriceItem = {
  currency: string;
  value: string;
};

export default ((props) => {
  const { salePrice, listPrice } = props || {};
  return (
    <Box d="flex">
      <Text textDecoration={salePrice?.value ? "line-through" : ""}>
        {listPrice.value} {listPrice.currency}
      </Text>
      {salePrice?.value && (
        <Text fontWeight="bold" color="red" ml="2">
          {salePrice.value} {salePrice.currency}
        </Text>
      )}
    </Box>
  );
}) as React.FunctionComponent<Price>;
