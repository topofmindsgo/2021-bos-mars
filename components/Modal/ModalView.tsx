import React from "react";
import {useRouter} from "next/router";
import {modalContentHelper} from "../../Helpers/modalContent";
import {
    Modal,
    ModalOverlay,
    ModalContent,
    ModalHeader,
    ModalBody,
    ModalCloseButton,
} from "@chakra-ui/react";

const ModalView = () => {
    const router = useRouter();
    const {id, returnHref, modalContent, modalTitle} = router.query;

    const modalData: ModalType = {
        id: id,
        modalTitle: modalTitle,
        modalContent: modalContent,
        returnHref: returnHref
    }

    return (<>
        <Modal
            isOpen={!!modalData.returnHref && !!modalData.modalContent}
            onClose={() => router.push(`${returnHref}`)}
            motionPreset="slideInBottom"
            size="5xl"
        >
            <ModalOverlay/>
            <ModalContent py={3}>

                {modalData.modalTitle &&
                    <ModalHeader>
                        {modalData.modalTitle}
                    </ModalHeader>
                }

                <ModalBody mb={6}>
                    {modalContentHelper(modalData.modalContent, modalData.id)}
                </ModalBody>

                <ModalCloseButton />
            </ModalContent>
        </Modal>
    </>)
}

export default ModalView;