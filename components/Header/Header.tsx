import React from "react";
import MiniCart from "../MiniCart/MiniCart";
import {Container, Box} from "@chakra-ui/layout";
import {Image} from "@chakra-ui/react";

const AppHeader: React.FunctionComponent = () => {
    return(
        <>
            <Box background="elements.header.background">
                <Container maxW="container.xl">
                    <Box height="20" d="flex" mx="2" alignItems="center" justifyContent="space-between">

                        <Box>
                            <Image src="/images/logo.svg" maxW={["100px", "200px"]} height="40px" className="logo" />
                        </Box>

                        <Box>
                            <MiniCart />
                        </Box>

                    </Box>
                </Container>
            </Box>
        </>
    )
}

export default AppHeader;