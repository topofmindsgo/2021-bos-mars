import React from "react";
import { SimpleGrid } from "@chakra-ui/react";
import ProductCard from "../ProductCard/ProductCard";

declare type Props = {
  products: [];
};

export default (({ products }) =>
  products?.length > 0 ? (
    <SimpleGrid columns={[1, 2, 3, 4]} spacing={2} my="2">
      {products.map((product, index) => (
        <ProductCard key={`product-card-${index}`} {...product} />
      ))}
    </SimpleGrid>
  ) : (
    <></>
  )) as React.FunctionComponent<Props>;
