import React, { useState } from "react";
import { useRouter } from "next/router";
import { useContextualRouting } from "next-use-contextual-routing";
import { Image } from "@chakra-ui/react";
import ProductPrice from "../ProductPrice/ProductPrice";
import labelTheme from "../../Helpers/labelTheme";
import { AspectRatio, Box, Badge, Text, Stack } from "@chakra-ui/react";
import { MODAL_CONTENT_PRODUCT } from "../../types/types";

declare type Product = {
  title: string;
  description: string;
  attributes: Array<Attribute>;
};

declare type Attribute = {
  name: string;
  value: string;
};

export default ((props) => {
  const { title, description, attributes } = props;
  console.log(props);
  const router = useRouter();
  const { makeContextualHref, returnHref } = useContextualRouting();
  const sku = attributes?.find((x) => x.name === "sku")?.value;

  const onProductCardClick = () =>
    router.push(
      makeContextualHref({
        modalContent: MODAL_CONTENT_PRODUCT,
        id: sku,
        returnHref,
      }),
      `/product/${sku}`,
      {
        shallow: true,
      }
    );

  return (
    <>
      <Box
        borderWidth="1px"
        overflow="hidden"
        position="relative"
        cursor="pointer"
        onClick={onProductCardClick}
      >
        <AspectRatio maxW="400px" ratio={4 / 3}>
          <Image
            src={`http://192.168.1.84${
              attributes?.find((x) => x.name === "image")?.value
            }`}
            alt={title}
            objectFit="cover"
          />
        </AspectRatio>

        <Box m="4" mt="2">
          <Box isTruncated fontWeight="bold">
            {title}
          </Box>

          <Text isTruncated textDecoration="underline">
            {attributes.find((x) => x.name === "manufacturer").value}
          </Text>
          <Text isTruncated>{description}</Text>

          <Box>
            <ProductPrice
              {...{
                salePrice: attributes?.find((x) => x.name === "salePrice")
                  ?.value,
                listPrice: attributes?.find((x) => x.name === "listPrice")
                  ?.value,
              }}
            />
          </Box>
        </Box>
      </Box>
    </>
  );
}) as React.FunctionComponent<Product>;
