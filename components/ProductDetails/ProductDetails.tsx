import React, { useState } from "react";
import { useDispatch } from "react-redux";
import { cartActions, toastActions } from "../../actions";
import {
  AspectRatio,
  Box,
  Badge,
  Text,
  Stack,
  HStack,
  Button,
  Heading,
  List,
  ListItem,
  ListIcon,
  UnorderedList,
} from "@chakra-ui/react";
import { Image, Select } from "@chakra-ui/react";
import labelTheme from "../../Helpers/labelTheme";
import Icon from "../../Helpers/Icon";

declare type Props = {
  id: string;
};

const ProductDetails: React.FunctionComponent<Props> = (props) => {
  const {
    unit,
    manufacturer,
    shortDescription,
    longDescription,
    sku,
    images,
    listPrice,
    availableStock,
    availability,
    name,
    packingUnit,
    attributes,
  } = props?.sku || {};

  // Temporary stuff
  const hasCampaign = false;

  const dispatch = useDispatch();
  const [activeImage, setActiveImage] = useState(
    images ? images[0].effectiveUrl : null
  );
  const [selectedSize, setSelectedSize] = useState(null);
  const thumbnailSize = "65px";

  const onSelectSizeChange = (event) => {
    const size = event.target.value;
    setSelectedSize(size);
  };

  const onAddToCartClick = () => {
    dispatch(cartActions.add(props, 1));
    dispatch(
      toastActions.show({
        title: `${name} was added to your cart`,
        status: "success",
      })
    );

    /*
        if (selectedSize) {
            dispatch(cartActions.add(props, 1))
            dispatch(toastActions.show({ title: `${name} was added to your cart`, status: "success" }));
        } else {
            dispatch(toastActions.show({ title: 'Please select a size', status: 'error' }));
        }
        */
  };

  return (
    <>
      <Box d="flex" flexDirection={["column", "row"]}>
        <Box mr={4} width={["100%", "60%"]}>
          <AspectRatio
            height={["250px", "400px", "500px", "600px"]}
            ratio={4 / 3}
          >
            <Image src={activeImage} alt={name} objectFit="cover" />
          </AspectRatio>

          <Box height={thumbnailSize} d="flex" mt="2">
            {
              // Image thumbnails
              images?.length > 1 &&
                images.map((image) => (
                  <Image
                    src={image.effectiveUrl}
                    boxSize={thumbnailSize}
                    borderRadius={5}
                    key={image.effectiveUrl}
                    alt={name}
                    onMouseEnter={() => setActiveImage(image)}
                    mr="2"
                    opacity={activeImage === image ? "1" : ".75"}
                  />
                ))
            }
          </Box>

          {attributes && attributes.length > 0 && (
            <List spacing={3} mt={4}>
              {attributes.map((attribute) => (
                <ListItem key={attribute.name}>
                  <strong>{attribute.name}</strong> {attribute.value}
                </ListItem>
              ))}
            </List>
          )}

          <Text mt={6}>{longDescription}</Text>
        </Box>

        <Box>
          <Box mb={3}>
            <Text fontWeight="bold">{manufacturer}</Text>
            <Heading as="h2" size="lg">
              {name}
            </Heading>

            <Text mt={6}>{shortDescription}</Text>
          </Box>

          <Stack direction="row" mb={3}>
            {
              // Labels
              /*
                        labels?.length >= 1 &&
                        labels.map(label =>
                        (<Badge key={label.id}
                            mr="2"
                            px="2"
                            colorScheme={labelTheme(label.type)}>
                            {label.name}
                        </Badge>))
                         */
            }
          </Stack>

          <Box
            d="flex"
            flexDirection={["column"]}
            alignItems="flex-start"
            mb={3}
            lineHeight="1"
          >
            <Text
              textDecoration={hasCampaign ? "line-through" : ""}
              color={hasCampaign ? "gray.400" : "inherit"}
              fontSize={hasCampaign ? 24 : 36}
            >
              {listPrice.value} {listPrice.currency}
            </Text>
            {hasCampaign && (
              <Text fontSize={36} fontWeight="bold" color="red">
                {listPrice.value} {listPrice.currency}
              </Text>
            )}
          </Box>

          <Box>
            {
              // Product variations (size, color etc)
              /*
                        Object.keys(variants).map((key, value) =>
                            <Select size="lg" mb={3} placeholder={`Select ${key}`} key={key} onChange={onSelectSizeChange}>
                                {
                                    variants[key].map((variant) =>
                                        <option key={variant.id} value={variant.value} disabled={!variant.inStock}>Size {variant.value} {!variant.inStock ? ' - out of stock' : ''}</option>
                                    )
                                }
                            </Select>)
                         */
            }

            <Button onClick={onAddToCartClick} colorScheme="green" mb={3}>
              Add to cart
            </Button>

            <HStack spacing={4}>
              {availability ? (
                <Box d="flex">
                  <Icon name="check" />
                  <Text ml={1}>In stock</Text>
                </Box>
              ) : (
                <Box d="flex">
                  <Icon name="close" />
                  <Text ml={1}>Out of stock</Text>
                </Box>
              )}

              <Box d="flex">
                <Icon name="check" />
                <Text ml={1}>Delivery in 1-3 days</Text>
              </Box>

              <Box d="flex">
                <Icon name="check" />
                <Text ml={1}>30 days free return</Text>
              </Box>
            </HStack>
          </Box>
        </Box>
      </Box>
    </>
  );
};

export default ProductDetails;
