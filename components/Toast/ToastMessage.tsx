import React, {useEffect} from "react";
import {useSelector} from "react-redux";
import {useToast} from "@chakra-ui/toast";
import {TOAST_CLEAR, TOAST_SHOW} from "../../types/types";

const ToastMessage:React.FunctionComponent = () => {
    const toastMessage = useSelector((state) => (state.toast));
    const toast = useToast();

    const handleToast = () => {
        switch (toastMessage.action) {
            case TOAST_SHOW:
                return toast({...toastMessage.lastToast, duration: 4500, isClosable: true});
            case TOAST_CLEAR:
                return toast.closeAll();
        }
    }

    useEffect(() => {handleToast()}, [toastMessage.lastToast])

    return <></>
}

export default ToastMessage;