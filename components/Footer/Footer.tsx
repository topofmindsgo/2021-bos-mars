import React from "react";
import { Box, Text } from "@chakra-ui/react";
import {Container} from "@chakra-ui/react";

const AppFooter: React.FunctionComponent = () => {
    return(
        <Box background="gray.700" color="gray.400" py="4">
            <Container maxW="container.xl">
                <footer>
                    <Text>
                        Storefront accelerator by Top of Minds Go
                    </Text>
                </footer>
            </Container>
        </Box>
    )
}

export default AppFooter;