import React from "react";
import {useSelector} from "react-redux";
import {Box, Badge, Text} from "@chakra-ui/layout";
import {Drawer, DrawerBody, DrawerCloseButton, DrawerContent, DrawerHeader, DrawerOverlay, DrawerFooter} from "@chakra-ui/modal";
import {useDisclosure} from "@chakra-ui/hooks";
import { useMediaQuery, Button } from "@chakra-ui/react"
import appTheme from "../../styles/theme";
import Icon from "../../Helpers/Icon";
import MiniCartItem from "../MiniCartItem/MiniCartItem";

const MiniCart: React.FunctionComponent = () => {
    const cart = useSelector((state) => state.cart);
    const { isOpen, onOpen, onClose } = useDisclosure();
    const [isLargerThan768] = useMediaQuery("(min-width: 768px)")
    const badgeStyle = () => ({
        width: cart.quantity <= 9 ? '18px' : '22px',
        height: cart.quantity <= 9 ? '18px' : '22px',
        top: cart.quantity <= 9 ? '-8px' : '-12px',
        right: cart.quantity <= 9 ? '-8px' : '-8px'
    })

    return (
        <>
            <Box onClick={onOpen} d="flex" cursor="pointer">
                <Box position="relative">
                    <Icon name="cart" size="lg" color={appTheme.colors.elements.header.color} />
                    {cart.quantity > 0 &&
                        <Badge position="absolute"
                               top={badgeStyle().top}
                               right={badgeStyle().right}
                               borderRadius="full"
                               minWidth={badgeStyle().width}
                               minHeight={badgeStyle().height}
                               d="flex"
                               justifyContent="center"
                               alignItems="center"
                               fontSize="xs"
                               colorScheme="brand"
                               transition=".15s ease all"
                        >{cart.quantity}</Badge>
                    }
                </Box>

                { cart.quantity > 0 && isLargerThan768 &&
                    <Text color="white" ml="4" fontWeight="bold" fontSize="lg">{cart.sum} {cart.currency}</Text>
                }
            </Box>

            <Drawer isOpen={isOpen} placement="right" onClose={onClose}>
                <DrawerOverlay>
                    <DrawerContent>
                        <DrawerCloseButton />
                        <DrawerHeader>My cart</DrawerHeader>
                        <DrawerBody>
                            {!cart?.items?.length ?
                                <>
                                    <Text>Your shopping cart is empty.</Text>
                                </>
                                :
                                <>
                                    {cart.items.map(cartItem => <MiniCartItem {...cartItem} key={cartItem.sku} />)}
                                </>
                            }
                        </DrawerBody>
                        <DrawerFooter justifyContent="center">
                            {cart?.items?.length > 0 &&
                                <Box>
                                    <Text fontSize="xl" fontWeight="bold" mb={4}>
                                        Total: {cart?.sum} {cart?.currency}
                                    </Text>

                                    <Button size="lg" colorScheme="green">Continue to checkout</Button>
                                </Box>
                            }
                        </DrawerFooter>
                    </DrawerContent>
                </DrawerOverlay>
            </Drawer>
        </>
    )
}

export default MiniCart;