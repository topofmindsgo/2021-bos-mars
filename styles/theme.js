import {fontFamily} from "./Fonts";

export default {
    colors: {
        brand: {
            50: '#fff3dc',
            100: '#ffddb0',
            200: '#fec680',
            300: '#fcb04f',
            400: '#fb991f',
            500: '#e28008',
            600: '#b06303',
            700: '#7e4700',
            800: '#4c2a00',
            900: '#1e0c00'
        },
        elements: {
            header: {
                background: '#383838',
                color: '#fff'
            }
        }
    },

    fonts: {
        heading: `${fontFamily}`,
        body: `${fontFamily}`
    }

}