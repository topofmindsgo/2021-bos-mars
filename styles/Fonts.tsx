import {Global} from "@emotion/react";

export const fontFamily = "Open Sans";

const Fonts = () => (
    <Global
        styles={`
            @font-face {
                font-family: ${fontFamily};
                src: url('/fonts/OpenSans-Regular.woff') format('woff2'),
                     url(/fonts/OpenSans-Regular.woff') format('woff');
                font-weight: normal;
                font-style: normal;
                font-display: swap;
            }
            
            @font-face {
                font-family: ${fontFamily};
                src: url('/fonts/OpenSans-Bold.woff') format('woff2'),
                    url('/fonts/OpenSans-Bold.woff') format('woff');
                font-weight: bold;
                font-style: normal;
                font-display: swap;
            }
            
            @font-face {
                font-family: ${fontFamily};
                src: url('/fonts/OpenSans-Light.woff2') format('woff2'),
                    url('/fonts/OpenSans-Light.woff2') format('woff');
                font-weight: 300;
                font-style: normal;
                font-display: swap;
            }
        `}
    />
)

export default Fonts;