import React, { useState } from "react";
import { useRouter } from "next/router";
import { Container } from "@chakra-ui/layout";
import { GET_PRODUCT } from "../../queries/product";
import ProductDetail from "../../components/ProductDetails/ProductDetails";
import { initializeApollo } from "../../lib/apolloClient";

interface Props {
  id: string;
}

const Product: React.FunctionComponent<Props> = (props) => {
  const product = props;
  return (
    <>
      <Container maxW="container.xl">
        {product && <ProductDetail sku={product} />}
      </Container>
    </>
  );
};

export default Product;

const apolloClient = initializeApollo();

export async function getStaticProps({ params: { id } }) {
  const { data, errors } = await apolloClient.query({
    query: GET_PRODUCT,
    variables: { id },
  });

  return {
    props: { ...data.getProductSingle },
  };
}

export async function getStaticPaths() {
  return {
    paths: [{ params: { id: "" } }],
    fallback: true,
  };
}
