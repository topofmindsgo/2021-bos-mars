import type {AppProps} from "next/app";
import {Provider} from "react-redux";
import {useStore} from "../store";
import {ApolloClient, InMemoryCache, gql, ApolloProvider} from '@apollo/client';
import { useApollo } from "../lib/apolloClient";
import Layout from '../components/Layout';
import { ChakraProvider } from "@chakra-ui/react"
import { extendTheme } from "@chakra-ui/react";
import Fonts from "../styles/Fonts";
import appTheme from "../styles/theme";
import Head from "next/head";
import React from "react";

const theme = extendTheme(appTheme);

const StoreFrontAccelerator = ({ Component, pageProps }: AppProps) => {
    const store = useStore(pageProps.initialReduxState);
    const apolloClient = useApollo(pageProps);

    return (
        <ApolloProvider client={apolloClient}>
            <ChakraProvider theme={theme}>
                <Fonts />
                <Head>
                    <title>Storefront accelerator</title>
                    <link rel="icon" href="/favicon.png" />
                </Head>
                <Provider store={store}>
                    <Layout>
                        <Component {...pageProps} />
                    </Layout>
                </Provider>
            </ChakraProvider>
        </ApolloProvider>
    )
}

export default StoreFrontAccelerator
