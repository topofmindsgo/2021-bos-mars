import React from "react";
import { Container } from "@chakra-ui/layout";
import ProductGrid from "../components/ProductGrid/ProductGrid";
import { ALL_PRODUCTS } from "../queries/product";
import { initializeApollo } from "../lib/apolloClient";

declare type Props = {
  elements: [];
};

export default (({ elements }) => (
  <Container maxW="container.xl">
    <ProductGrid {...{ products: elements }} />
  </Container>
)) as React.FunctionComponent<Props>;

const apolloClient = initializeApollo();
export async function getStaticProps(ctx) {
  const { data, errors } = await apolloClient.query({
    query: ALL_PRODUCTS,
  });
  const { getProductsListResource } = data || {};

  return {
    props: { ...getProductsListResource },
  };
}
